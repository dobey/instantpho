/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Rectangle {
    id: rootItem
    anchors.fill: parent

    color: Qt.rgba(0, 0, 0, 0.5)

    MouseArea {
        anchors.fill: parent
        // Don't propagate events
    }

    LomiriShape {
        anchors.centerIn: parent
        width: row.childrenRect.width + units.gu(16)
        height: units.gu(10)

        backgroundColor: theme.palette.normal.raised
        aspect: LomiriShape.Inset

        Row {
            id: row
            anchors.centerIn: parent
            anchors.horizontalCenterOffset: units.gu(-2)
            spacing: units.gu(2)

            ActivityIndicator {
                anchors.verticalCenter: parent.verticalCenter
                running: true
            }

            Label {
                anchors.verticalCenter: parent.verticalCenter
                text: i18n.tr("Processing image...")
            }
        }
    }
}

