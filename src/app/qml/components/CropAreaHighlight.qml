/*
 * Copyright (C) 2014, 2015, 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Lomiri.Components 1.3

Item {
    id: rootItem

    property alias rect: rect
    property alias cropRatio: rect.ratio

    property bool showGrid: true

    Rectangle {
        id: rect

        property bool __isPortrait: rootItem.width < rootItem.height
        property real ratio: 1

        width: __isPortrait
                    ? (parent.width / ratio) > parent.height
                        ? parent.height * ratio
                        : parent.width
                    : (parent.height * ratio) > parent.width
                        ? parent.width
                        : (parent.height * ratio)

        height: __isPortrait
                    ? (parent.width / ratio) > parent.height
                        ? parent.height
                        : (parent.width / ratio)
                    : (parent.height * ratio) > parent.width
                        ? parent.width / ratio
                        : parent.height


        anchors.centerIn: parent

        color: "transparent"
     //   color: "red"

        GridView {
            id: gridView
            anchors.fill: parent
            visible: showGrid
            interactive: false

            model: 9
            cellWidth: parent.width / 3
            cellHeight: parent.height / 3
            delegate: Item {
                width: gridView.cellWidth
                height: gridView.cellHeight
                opacity: 0.6

                Rectangle {
                    anchors {
                        top: parent.top
                        right: parent.right
                        bottom: parent.bottom
                    }
                    width: units.dp(1)
                    color: "white"
                }

                Rectangle {
                    anchors {
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }
                    height: units.dp(1)
                    color: "white"
                }
            }
        }

        Rectangle {
            anchors { fill: parent }
            color: "transparent"
            border {
                color: "white" //theme.palette.selected.backgroundText//"#eeeeee"
                width: units.dp(2)
            }
        }
    }
}
