/*
 * Copyright (C) 2014, 2015, 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Lomiri.Components 1.3

Item {
    id: rootItem

    property var overlay

    property alias source: img.source
    property alias horizonRotation: img.horizonRotation
    property alias sourceSize: img.sourceSize

    property alias zoomFactor: img.uscale

    readonly property alias originalImageRatio: img.ratio

    function getCropRect() {
        var result = {
            "x" : imgFlickable.contentX / img.width,
            "y" : imgFlickable.contentY / img.height,
            "width" : overlay.rect.width / img.width,
            "height" : overlay.rect.height / img.height
        }

        return result
    }

    function importParameters(sourcePath, x, y, zoom, cropRatio) {
        img.source = sourcePath
        rootItem.zoomFactor = zoom
        rootItem.overlay.cropRatio = cropRatio
        imgFlickable.contentX = x
        imgFlickable.contentY = y
    }

    function exportParameters() {
        return {
            "sourcePath": img.source,
            "zoom" : rootItem.zoomFactor,
            "cropRatio" : rootItem.overlay.cropRatio,
            "x" : imgFlickable.contentX,
            "y" : imgFlickable.contentY
        }
    }

    onWidthChanged: img.centerImage()
    onHeightChanged: img.centerImage()

    ScrollView {
        anchors.fill: parent
        viewport.clip: false

        Flickable {
            id: imgFlickable
            anchors.fill: parent

            boundsBehavior: Flickable.StopAtBounds

            contentHeight: img.paintedHeight
            contentWidth: img.paintedWidth

            Image {
                id: img
                fillMode: Image.PreserveAspectFit

                property int horizonRotation: 0
                rotation: horizonRotation

                property real uscale: 1.0
                property real ratio: img.sourceSize.width / img.sourceSize.height

                smooth: !pinchArea.pinch.active
                cache: false

                property bool __cropRatioIsBiggerThanImgRatio: overlay.cropRatio > img.ratio

                width: __cropRatioIsBiggerThanImgRatio
                       ? overlay.rect.width * uscale
                       : overlay.rect.height * ratio * uscale
                height: __cropRatioIsBiggerThanImgRatio
                        ? overlay.rect.width / ratio * uscale
                        : (overlay.rect.height) * uscale

                function reset() {
                    img.uscale = 1.0;
                    centerImage()
                }

                function centerImage() {
                    imgFlickable.contentX = ((img.width - overlay.rect.width) / 2)
                    imgFlickable.contentY = ((img.height - overlay.rect.height) / 2)
                }

                Component.onCompleted: {
                    centerImage()
                }

                scale: {
                    var pi = 3.141592653589793
                    var r = (2 * pi) / 360 * Math.abs(rotation)

                    var sinRot = Math.sin(r)
                    var cosRot = Math.cos(r)
                    var a = img.height * sinRot
                    var d = img.height * cosRot
                    var i = Math.sqrt((a*a) + (d*d))
                    var alt = i * sinRot * cosRot

                    var s = (img.width + 2 * alt) / img.width

                    return s
                }
            }

            ScalingPinchArea {
                id: pinchArea
                anchors.fill: parent

                minimumZoom: 1.0
                maximumZoom: 5.0

                targetFlickable: imgFlickable
                onTotalScaleChanged: img.uscale = totalScale

                MouseArea {
                    anchors.fill: parent
                    onDoubleClicked: img.reset()
                }

                Binding {
                     when: !pinchArea.pinch.active
                     target: pinchArea
                     property: "zoomValue"
                     value: img.uscale
                 }
            }
        }
    }
}
