/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1
import InstantFX.ImageProcessor 0.1

Page {
    id: filterSidebarPage

    property var proc

    header: FunctionSelector {
        id: functionSelector
        model: [ i18n.tr("Filters"), i18n.tr("Effects") ]
    }
    clip: true

    Loader {
        id: functionViewLoader
        anchors {
            top: functionSelector.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        sourceComponent: functionSelector.selectedIndex == 0 ? filtersViewComponent : otherActionsViewComponent
        asynchronous: true

        Component {
            id: filtersViewComponent
            Item {
                anchors.fill: parent

                ScrollView {
                    anchors {
                        top: parent.top
                        bottom: filterLevelSliderContainer.top
                        left: parent.left
                        right: parent.right
                    }

                    clip: true

                    GridView {
                        id: filtersView
                        anchors.fill: parent
                        model: filtersList

                        Component.onCompleted: {
                            // WORKAROUND: Fix for wrong grid unit size
                            flickDeceleration = 1500 * units.gridUnit / 8
                            maximumFlickVelocity = 2500 * units.gridUnit / 8
                        }

                        topMargin: units.gu(2)
                        bottomMargin: units.gu(2)
                        leftMargin: units.gu(2)
                        cellWidth: units.gu(13)
                        cellHeight: units.gu(17)
                        delegate: AbstractButton {
                            id: filterDelegate
                            property bool isSelected: proc.filterUrl == Qt.resolvedUrl("../filters/") + model.fileName

                            width: units.gu(12)
                            height: units.gu(16)

                            onClicked: {
                                if (!isSelected) {
                                    filtersView.currentIndex = model.index
                                    proc.filterOpacity = 1.0
                                    proc.filterUrl = Qt.resolvedUrl("../filters/") + model.fileName
                                }
                            }

                            Rectangle {
                                anchors.fill: parent
                                color: theme.palette.selected.base
                                opacity: 0.5
                                visible: filterDelegate.isSelected
                            }

                            Rectangle {
                                anchors.fill: parent
                                color: theme.palette.highlighted.background
                                visible: filterDelegate.pressed
                            }

                            Column {
                                spacing: units.gu(0.5)
                                anchors {
                                    left: parent.left
                                    right: parent.right
                                    verticalCenter: parent.verticalCenter
                                }

                                Item {
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    width: Math.min(filtersView.height - units.gu(2), units.gu(10))
                                    height: width

                                    Rectangle {
                                        anchors.fill: parent
                                        color: theme.palette.normal.base
                                    }

                                    Image {
                                        id: previewImg
                                        anchors.fill: parent

                                        sourceSize.width: width * 0.5
                                        sourceSize.height: height * 0.5
                                        fillMode: Image.PreserveAspectCrop

                                        source: proc.loadedImagePath
                                        asynchronous: true
                                        visible: false
                                    }

                                    Loader {
                                        anchors.fill: parent
                                        source: Qt.resolvedUrl("../filters/") + model.fileName
                                        asynchronous: true
                                        onLoaded: {
                                            if (item)
                                                item.img = previewImg
                                        }
                                    }
                                }

                                Label {
                                    anchors { left: parent.left; right: parent.right }
                                    horizontalAlignment: Text.AlignHCenter
                                    textSize: Label.Small
                                    text: model.name
                                }
                            }
                        }
                    }
                }

                Item {
                    id: filterLevelSliderContainer
                    height: visible ? units.gu(12) : 0
                    anchors {
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }

                    visible: proc.filterUrl !=  Qt.resolvedUrl("../filters/") + filtersList.get(0).fileName
                             && proc.filterUrl != ""
                    enabled: visible

                    onVisibleChanged: {
                        if (visible) {
                            // This item get visible and overlay a portion of the grid view.
                            // Move the content of the view up by the height of this panel.
                            filtersView.contentY += units.gu(12)
                        }
                    }

                    Behavior on height { LomiriNumberAnimation { duration: LomiriAnimation.SnapDuration } }

                    Rectangle {
                        anchors {
                            left: parent.left
                            right: parent.right
                            top: parent.top
                        }
                        height: units.dp(1)
                        color: theme.palette.normal.base
                    }


                    Column {
                        anchors {
                            left: parent.left
                            right: parent.right
                            verticalCenter: parent.verticalCenter
                            verticalCenterOffset: units.gu(-1)
                        }

                        ListItemLayout {
                            anchors { left: parent.left; right: parent.right }
                            subtitle.text: i18n.tr("Filter opacity level:")
                            height: units.gu(4)
                        }

                        Slider {
                            id: filterOpacitySlider
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width - units.gu(4)

                            minimumValue: 0.0
                            maximumValue: 1.0
                            live: true

                            function formatValue(v) { return (v * 100).toFixed(0) }

                            value: proc.filterOpacity
                            onValueChanged: proc.filterOpacity = value

                            Connections {
                                target: proc
                                onFilterUrlChanged: filterOpacitySlider.value = proc.filterOpacity
                            }

                            style: SliderStyle {}
                        }
                    }
                }
            }
        }

        Component {
            id: otherActionsViewComponent

            ScrollView {
                anchors.fill: parent
                clip: true
                property ImageProcessor imageHandler: proc

                ListView {
                    id: otherActionsView
                    anchors.fill: parent
                    model: effectList.model

                    Component.onCompleted: {
                        // WORKAROUND: Fix for wrong grid unit size
                        flickDeceleration = 1500 * units.gridUnit / 8
                        maximumFlickVelocity = 2500 * units.gridUnit / 8
                    }

                    delegate: ListItem {
                        id: otherActionsDelegate
                        expansion.height: units.gu(12)
                        onClicked: expansion.expanded = !expansion.expanded

                        ListItemLayout {
                            id: delegateLayout
                            property bool isSelected: proc.getProperty(modelData.prop) != modelData.defaultValue
                            title.text: modelData.name
                            summary.text: "Current value: " + proc.getProperty(modelData.prop)
                            height: otherActionsDelegate.implicitHeight
                            //selected: imageHandler.getProperty(modelData.prop) != modelData.defaultValue
                            Icon {
                                SlotsLayout.position: SlotsLayout.Leading
                                width: units.gu(4); height: width
                                color: theme.palette.normal.foregroundText
                                source: modelData.iconName ? "image://theme/%1".arg(modelData.iconName) : modelData.iconSource
                            }

                            Icon {
                                height: units.gu(2); width: height
                                name: "go-up"
                                SlotsLayout.position: SlotsLayout.Last
                                rotation: otherActionsDelegate.expansion.expanded ? 0 : 180
                                Behavior on rotation { LomiriNumberAnimation { duration: LomiriAnimation.SnapDuration } }
                            }

                            Connections {
                                target: proc
                                onImageSettingsChanged: {
                                    delegateLayout.isSelected = (proc.getProperty(modelData.prop) != modelData.defaultValue)
                                    delegateLayout.summary.text = "Current value: " + proc.getProperty(modelData.prop)
                                }
                            }
                        }

                        Loader {
                            active: otherActionsDelegate.expansion.expanded
                            anchors.fill: parent

                            sourceComponent: RowLayout {
                                anchors {
                                    left: parent ? parent.left : undefined
                                    right: parent ? parent.right : undefined
                                    margins: units.gu(2)
                                    verticalCenter: parent ? parent.verticalCenter : undefined
                                    verticalCenterOffset: units.gu(2)
                                }
                                height: units.gu(8)
                                spacing: units.gu(2)

                                Slider {
                                    Layout.fillWidth: true
                                    Layout.alignment: Qt.AlignVCenter

                                    minimumValue: modelData.minimumValue
                                    maximumValue: modelData.maximumValue
                                    live: true

                                    function formatValue(v) {
                                        return modelData.formatValue(v)
                                    }

                                    value: proc.getProperty(modelData.prop)
                                    onValueChanged: proc.setProperty(modelData.prop, value)

                                    property bool centeredOverlay: modelData.defaultValue > modelData.minimumValue && modelData.defaultValue < modelData.maximumValue
                                    property bool rightAlignedOverlay: modelData.defaultValue == modelData.maximumValue
                                    style: SliderStyle {}
                                }

                                AbstractButton {
                                    id: resetButton
                                    Layout.preferredHeight: units.gu(6)
                                    Layout.preferredWidth: units.gu(4)

                                    onClicked: {
                                        proc.setProperty(modelData.prop, modelData.defaultValue)
                                        otherActionsDelegate.expansion.expanded = false
                                    }

                                    Rectangle {
                                        color: LomiriColors.slate
                                        opacity: 0.1
                                        anchors.fill: parent
                                        visible: resetButton.pressed
                                    }

                                    Icon {
                                        anchors.centerIn: parent
                                        width: units.gu(2); height: width
                                        name: "reset"
                                        color: theme.palette.normal.backgroundText
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
