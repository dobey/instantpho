/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Lomiri.Components 1.3

AbstractButton {
    id: strokeButton

    property color color: "#b2b2b2"
    property color pressedColor: Qt.rgba(theme.palette.normal.base.r, theme.palette.normal.base.g, theme.palette.normal.base.b, 0.5)

    property string text
  //  property string iconSource
    property string iconName

    style: Item {
        width: strokeButton.width
        height: strokeButton.height
        implicitWidth: Math.max(units.gu(10), foreground.implicitWidth + units.gu(2))
        implicitHeight: units.gu(4)

        LayoutMirroring.enabled: Qt.application.layoutDirection == Qt.RightToLeft
        LayoutMirroring.childrenInherit: true
        opacity: strokeButton.enabled ? 1.0 : 0.6

        Rectangle {
            anchors.fill: parent
            color: strokeButton.pressed ? strokeButton.pressedColor : "transparent"
            radius: units.dp(4)

            border {
                width: units.dp(1)
                color: strokeButton.color
            }
        }

        Row {
            id: foreground
            spacing: units.gu(1)
            anchors {
                top: parent.top
                bottom: parent.bottom
                left: parent.left; leftMargin: units.gu(1)
                margins: units.dp(2)
            }

            Icon {
                id: icon
                anchors.verticalCenter: parent.verticalCenter
                color: strokeButton.color
                width: units.gu(3); height: width
                name: strokeButton.iconName
            }

            Label {
                id: label
                anchors {
                    verticalCenter: parent.verticalCenter
                    verticalCenterOffset: units.dp(1)
                }
                elide: Text.ElideRight
                text: strokeButton.text
                color: strokeButton.color
            }
        }
    }
}
