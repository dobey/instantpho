/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import InstantFX.ImageProcessor 0.1
import Lomiri.Content 1.3

import "components"

MainView {
    id: mainView

    property alias proc: proc
    property alias appStore: appStore
    property var cropTmp
    property var activeTransfer

    property bool isSavingImage: false

    readonly property bool isLandscape: width > height
    readonly property bool isWideScreen: (width > units.gu(120)) && isLandscape

    width: units.gu(50)
    height: units.gu(75)

    applicationName: "instantpho.dobey"

    Component.onCompleted: {
        i18n.domain = "instantpho"
        pageStack.push(Qt.resolvedUrl("ui/MainPage.qml"))
    }

    // 39ms
    ImageProcessor {
        id: proc

        // Default filter
        filterUrl: Qt.resolvedUrl("filters/NoFilter.qml")

        // TODO: Move to C++
        onFilterChanged: {
            //filter.width = __output.width
            //filter.height = __output.height
            filter.img = __output.__clarityFilter
            filter.parent = __output.__filterContainer
            filter.anchors.fill = parent
        }

        onImageSaved: {
            __output.setDefaultSize()

            isSavingImage = false

            console.log("file saved as", path);
            exportPeer.save(path)

            // FIXME: This should stay in 'ui/FiltersPage.qml'. Needs further investigation.
            // If the page which calls ImageProcessor::saveToDisk() is removed from the stack,
            // proc's image container is detached from its window.
            pageStack.clear()
            pageStack.push(Qt.resolvedUrl("ui/MainPage.qml"))
        }
    }

    PageStack {
        id: pageStack
    }

    ContentStore {
        id: appStore
        scope: ContentScope.App
    }

    ContentPeer {
        id: exportPeer
        // Default is gallery-app
        //appId: "gallery.ubports"
        contentType: ContentType.Pictures
        handler: ContentHandler.Destination

        property Component picItem: ContentItem {}

        function save(filePath) {
            var transfer = exportPeer.request()
            transfer.items = [ picItem.createObject(mainView, { "url": filePath }) ]
            transfer.state = ContentTransfer.Charged
        }
    }

    Loader {
        anchors.fill: parent
        sourceComponent: BusyIndicator {}
        active: isSavingImage
    }
}
