/*
 * Copyright © 2018 Rodney Dawes
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.7
import Lomiri.Components 1.3

import "../components"

Page {
    id: aboutPage

    header: PageHeader {
        title: i18n.tr("About")
        flickable: scrollView.flickableItem
    }

    ScrollView {
        id: scrollView
        anchors.fill: parent

        Column {
            width: scrollView.width
            spacing: units.gu(2)

            Item {
                width: 1; height: units.gu(4)
            }

            Icon {
                anchors.horizontalCenter: parent.horizontalCenter
                width: units.gu(16); height: width
                source: Qt.resolvedUrl("../graphics/instantfx-symbolic.svg")
            }

            Column {
                width: parent.width

                Label {
                    fontSize: "x-large"
                    font.weight: Font.DemiBold
                    text: "InstantPho"

                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Label {
                    // TRANSLATORS: Version of the software (e.g. "Version 0.3.51")
                    text: i18n.tr("Version %1").arg(APP_VERSION)
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }

            Column {
                width: parent.width

                Label {
                    text: "© 2018 Rodney Dawes\n© 2016 Stefano Verzegnassi"
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Label {
                    fontSize: "small"
                    text: i18n.tr("Released under the terms of the GNU GPL v3")
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }

            Column {
                width: parent.width
                spacing: units.gu(2)

                Label {
                    fontSize: "small"
                    text: i18n.tr("Source code available on %1").arg("<a href=\"https://gitlab.com/dobey/instantpho\">GitLab</a>")

                    anchors.horizontalCenter: parent.horizontalCenter
                    onLinkActivated: Qt.openUrlExternally(link)
                    linkColor: LomiriColors.blue
                }
            }

            Column {
                anchors { left: parent.left; right: parent.right }

                SectionHeader {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    height: units.dp(2)
                }

                ListItem {
                    onClicked: Qt.openUrlExternally("https://gitlab.com/dobey/instantpho/issues")
                    ListItemLayout {
                        title.text: i18n.tr("Report a bug")
                        ProgressionSlot {}
                    }
                }
            }
        }
    }
}
