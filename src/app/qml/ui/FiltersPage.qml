/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import InstantFX.ImageProcessor 0.1

import "../filters" as Filters
import "../effects"
import "../components"

AdaptivePageLayout {
    id: filtersPageLayout
    anchors.fill: parent

    onColumnsChanged: doLayout()
    Component.onCompleted: {
        proc.filterUrl = Qt.resolvedUrl("../filters/NoFilter.qml")
        doLayout()
    }

    function doLayout() {
        if (columns > 1) {
            filtersPageLayout.addPageToNextColumn(filtersPage, Qt.resolvedUrl("../components/FilterSidebarPage.qml"), { proc: mainView.proc })
        } else {
            filtersPageLayout.removePages(filtersPage)
        }
    }

    layouts: [
        PageColumnsLayout {
            when: !mainView.isWideScreen
            PageColumn { fillWidth: true }
        },

        PageColumnsLayout {
            when: mainView.isWideScreen
            PageColumn { fillWidth: true }
            PageColumn { minimumWidth: units.gu(40); maximumWidth: units.gu(40); preferredWidth: units.gu(40) }
        }
    ]

    Filters.FiltersList { id: filtersList }
    EffectsList { id: effectList }

    Action {
        id: backAction
        text: i18n.tr("Back")
        onTriggered: {
            // Resource optimizations for low-end devices
            pageStack.clear()
            var cropPage = pageStack.push(Qt.resolvedUrl("CropPage.qml"))
            cropPage.__cropImg.importParameters(mainView.cropTmp.sourcePath, mainView.cropTmp.x, mainView.cropTmp.y, mainView.cropTmp.zoom, mainView.cropTmp.cropRatio)
        }
    }

    Action {
        id: exportAction
        text: i18n.tr("Done")
        onTriggered: {
            mainView.isSavingImage = true
            exportTimer.restart()
        }
    }

    Timer {
        id: exportTimer
        interval: 50   // This is just for a nice UX
        onTriggered: {
            var path = APP_PICTURES_LOCATION_PATH + "/" + new Date().valueOf() + ".jpg"

            if (!mainView.proc.saveToDisk(path, 80)) {
                console.log("ERROR", "File not saved!")
                return
            }
        }
    }

    primaryPage: Page {
        id: filtersPage
        anchors.fill: parent

        Loader {
            id: previewLoader
            anchors {
                top: filtersPage.header.bottom
                left: parent.left
                right: parent.right
                bottom: filtersContainer.top
                margins: filtersPageLayout.columns > 1 ? units.gu(2) : 0
                bottomMargin: filtersPageLayout.columns > 1 ? units.gu(2) + (filtersPage.header.height * 0.6) : 0
            }
            asynchronous: true
            sourceComponent: ImageProcessorOutput {
                anchors.fill: parent
                imageProcessor: proc
            }
        }

        Item {
            id: filtersContainer
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            visible: !mainView.isWideScreen
            height: visible ? units.gu(24) : 0

            FunctionSelector {
                id: functionSelector
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
                height: units.gu(6)

                model: [ i18n.tr("Filters"), i18n.tr("Effects") ]
            }

            Loader {
                id: functionViewLoader
                anchors {
                    top: functionSelector.bottom
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }

                sourceComponent: functionSelector.selectedIndex == 0 ? filtersView : otherActionsView
                asynchronous: true

                Component {
                    id: filtersView
                    FiltersView {
                        model: filtersList
                        imageHandler: proc
                    }
                }

                Component {
                    id: otherActionsView
                    OtherActionsView {
                        model: effectList.model
                        imageHandler: proc
                    }
                }
            }

            Loader {
                id: claritySettingsLoader
                anchors.fill: parent
                active: false
                onActiveChanged: {
                    if (active) {
                        // Close any filter setting panel from filters or other actions.
                        functionViewLoader.active = false
                        functionViewLoader.active = true
                    }
                }

                sourceComponent: ClaritySettingsPanel { proc: mainView.proc }
            }
        }

        Rectangle {
            anchors {
                left: parent.left
                right: parent.right
                top: filtersContainer.top
                topMargin: - height
            }
            height: units.dp(1)
            color: theme.palette.normal.base
        }

        header: PageHeader {
            leadingActionBar.anchors.leftMargin: 0
            leadingActionBar.delegate: TextualButtonStyle {}
            leadingActionBar.actions: backAction

            trailingActionBar.anchors.rightMargin: 0
            trailingActionBar.delegate: TextualButtonStyle {}
            trailingActionBar.actions: exportAction

            contents: AbstractButton {
                id: clarityButton
                anchors.centerIn: parent
                height: parent.height
                width: units.gu(6)

                onClicked: {
                    proc.clarity = 0.5

                    if (filtersPageLayout.columns == 1) {   // Phone layout
                        claritySettingsLoader.active = !claritySettingsLoader.active
                    } else {
                        // Desktop/tablet layout
                        PopupUtils.open(Qt.resolvedUrl("../components/ClaritySettingsPopover.qml"), clarityButton, { proc: mainView.proc })
                    }
                }

                Rectangle {
                    color: LomiriColors.slate
                    opacity: 0.1
                    anchors.fill: parent
                    visible: clarityButton.pressed
                }

                Label {
                    text: "\u2B24"
                    font.pixelSize: units.gu(1)
                    visible: proc.clarity != 0.0
                    anchors {
                        horizontalCenter: parent.horizontalCenter
                        bottom: clarityIcon.top
                    }
                }

                Icon {
                    id: clarityIcon
                    anchors.centerIn: parent
                    width: units.gu(3); height: width
                    name: "display-brightness-symbolic"
                    color: theme.palette.normal.backgroundText
                }
            }
        }
    }
}
