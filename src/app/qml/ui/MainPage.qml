/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import QtGraphicalEffects 1.0

import "../components"

Page {
    id: mainPage

    // Disable header
    header: Item {}

    Component.onCompleted: {
        mainView.isSavingImage = false
    }

    Image {
        id: bg
        anchors.fill: parent
        source: Qt.resolvedUrl("../graphics/wallpapers/1.jpg")
        sourceSize { width: bg.width; height: bg.height }
        fillMode: Image.PreserveAspectCrop
        visible: false
    }

    GaussianBlur {
        anchors.fill: bg
        source: bg
        samples: 8
        radius: 4
    }

    Rectangle {
        anchors {
            left: !mainView.isWideScreen ? parent.left : undefined
            top: parent.top
            right: parent.right
            bottom: parent.bottom
        }
        color: Qt.rgba(0.0, 0.0, 0.0, 0.25)
        width: Math.min(parent.width - units.gu(4), units.gu(60))

        Column {
            anchors.centerIn: parent

            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                height: units.gu(6)
                spacing: units.gu(1)

                Icon {
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height; width: height
                    color: "white"
                    source: Qt.resolvedUrl("../graphics/instantfx-symbolic.svg")
                }

                Label {
                    font.pixelSize: parent.height
                    text: "InstantPho"
                    color: "white"
                }
            }

            Item { width: 1; height: units.gu(4) }

            Label {
                anchors { left: parent.left; right: parent.right }
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
                text: i18n.tr("An application for Ubuntu devices.")
                color: "white"
            }

            Item { width: 1; height: units.gu(8) }

            Column {
                anchors { left: parent.left; right: parent.right }
                spacing: units.gu(2)

                StrokeButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: units.gu(32)
                    text: i18n.tr("Import a photo from...")
                    iconName: "gallery-app-symbolic"
                    color: "white"

                    onTriggered: {
                        var importPage = pageStack.push(Qt.resolvedUrl("ImportPage.qml"))

                        importPage.imported.connect(function(fileUrl) {
                            // Resource optimizations for low-end devices
                            pageStack.clear()

                            console.log("File" << fileUrl << "has been imported!")
                            pageStack.push(Qt.resolvedUrl("CropPage.qml"), { imageSource: fileUrl })
                        })
                    }
                }

                StrokeButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: units.gu(32)
                    text: i18n.tr("About this application")
                    iconName: "info"
                    color: "white"
                    onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                }
            }
        }

       /* MouseArea {
            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }
            height: units.gu(8)
            onClicked: {
                // Resource optimizations for low-end devices
                pageStack.clear()

                pageStack.push(Qt.resolvedUrl("CropPage.qml"), { imageSource: "file:///home/stefano/Scrivania/atx.jpg" })
            }
        }*/
    }
}
