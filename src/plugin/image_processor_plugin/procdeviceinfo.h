/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef PROCDEVICEINFO_H
#define PROCDEVICEINFO_H

#include <QObject>

class ProcDeviceInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int optimalPreviewSize READ optimalPreviewSize CONSTANT)

public:
    explicit ProcDeviceInfo(QObject *parent = 0);

    int optimalPreviewSize() const { return m_optimalPreviewSize; }

private:
    int m_optimalPreviewSize;
};

#endif // PROCDEVICEINFO_H
